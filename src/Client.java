/**
 * Created by hitesh on 15/10/15.
 */

import sun.net.TelnetOutputStream;

import java.io.*;
import java.net.*;

import java.util.*;

public class Client {

    public static void main(String[] args) {
        List<String> rawDataList=fileReadByLine();
        PrintWriter out;
        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress("localhost", 5555));
            //DataOutputStream dout = new DataOutputStream(s.getOutputStream());
            out = new PrintWriter(s.getOutputStream(), true);
            long start=System.currentTimeMillis();
            for (String str : rawDataList) {
                out.println(str);
                //Thread.sleep(5000);
            }
            long end=System.currentTimeMillis();
            System.out.println("millis=");
            System.out.print(end - start);
            System.out.println("second=");
            System.out.print((end-start)/60);

            /*dout.flush();
            dout.close();*/
            s.close();
        } catch (Exception e) {
            System.out.println("error=" + e);
        }
        System.out.println("Finish...");
    }

    public static List<String> fileReadByLine() {
        List<String> rawDataList=new ArrayList<String>();
        BufferedReader br = null;
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader("config"));
            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println("line="+sCurrentLine);
                rawDataList.add(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("total no. of data="+rawDataList.size());
        return rawDataList;
    }
}